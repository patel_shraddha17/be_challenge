# Back End Developer Exercise

The aim of this exercise is to simulate real working conditions to provide context for a code/design review session. The follow up review session will focus on your reasons for database/API design and pseudo-code/code implementation. As such it isn�t necessary to build a complete implementation, however having some runnable code is recommended (preferably in Python).

The suggested time to spend on this exercise is 2 hours.

### Instructions

For this challenge, we are looking for you to create the backend for a a simple rostering application for editing the shifts of employees. For example this application could be used for a small business that works 24/7 to manage the shifts of it's employees to make sure everyone gets adequate days off and doesn't get shifts which are directly back-to-back (eg working on a night shift followed by a morning shift the next day).

We're providing you with two mock data csv files which are typical of the type of data collected:

- Employees: The people who are being rostered
- Shifts: These are the bits of work assigned to employees.

### Solution 

Built a basic algorithm for scheduling as an optimization problem for following constraints:

- Multiple employees can be scheduled during any shift. So, the algorithm should ensure that an employee is not scheduled more than once in a given shift. 
- No employee should be scheduled in back-to-back or overlapping shifts. 
- Uniform distribution of shifts across all the employees so that no employee is over-worked. 

To address these constraints, a simple algorithm goes through a list of employees and finds the first employee who is not working in the same shift and has not worked in the previous shift.
Then this employee is put at the end of the list for scheduling for the next shift. This ensures uniform distribution across all the employees.

If any of the requirements are unclear feel free to send through questions for clarification or make assumptions - we are not trying to test you on your knowledge of rostering.

### Deliverable

A webapp has been built with minimal UI to upload csv for employees and shifts. It is available at the basic site url.
(eg. http://127.0.0.1:8000/)
The code is written in Python 3 and the framework used is Django. It has not been integrated with Postgresql.
All necessary error checks/validation have been implemented.

### Improvements

- Adding more contraints around shifts for instance shop closing, festivals etc. 
- Smart suggestions - Where do conflicts happen and how can we solve them. (Eg. if we can get more staff for a particular shift)
- Try to build a system where employees get regular day off/shifts off. This might aid in employee satisfaction and retention.


### Assumptions

- Shifts come in sorted by their start date/time. A bit more flexibility can be added by adding a dynamic functionality where shifts can be changed/dropped by employees on the fly.
- Every shift is of the same duration.
- The end of every shift overlaps or coincides exactly with the beginning of the next shift.
- Any employee can work any shift without the constraints of day/time.




