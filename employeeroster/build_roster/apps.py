from django.apps import AppConfig


class BuildRosterConfig(AppConfig):
    name = 'build_roster'
