import pandas as pd
import numpy as np
from datetime import timedelta

def process_shifts(employees,shifts):
    roster = []
    try:
        shifts['Start'] = pd.to_datetime(shifts['Date'] + ' ' + shifts['Start'])
        shifts['End'] = pd.to_datetime(shifts['Date'] + ' ' + shifts['End'])
        shifts['End'] = np.where(shifts['End'] > shifts['Start'], shifts['End'], shifts['End'] + timedelta(days=1))
        employees_arr = []
        for index, row in employees.iterrows():
            employees_arr.append(Employee(row['First Name'],row['Last Name']))

        for index, row in shifts.iterrows():
            shift  = (Shift(row['Start'],row['End'],row['Break']))
            found=False
            for employee in employees_arr:
                if(employee.check_shift(shift)):
                    employee.add_shift(shift)
                    roster.append(str(shift)+' '+str(employee))
                    #rotates the roster
                    employees_arr = employees_arr[1:] + employees_arr[:1]
                    found=True
                    break
            if not found:
                #if no employee available for the shift
                roster = []
                break
        # check the number of shifts for each employee
        # for employee in employees_arr:
        #    print(str(employee)+' '+str(len(employee.shifts)))
    except:
        roster = []
    return roster



class Shift:
    'Class for storing and processing shifts'
    def __init__(self,start_time,end_time,break_time):
        self.start_time = start_time
        self.end_time = end_time
        self.break_time = break_time
        self.duration = self.end_time - self.start_time

    def __str__(self):
        return str(self.start_time)+' '+str(self.end_time)

    def __eq__(self, other):
        if type(other) is type(self):
            return self.__dict__ == other.__dict__
        return False

class Employee:
    'Storing employee details and shift details'
    def __init__(self,first_name,last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.shifts = []

    def add_shift(self,shift):
        self.shifts.append(shift)

    def check_shift(self,shift):
        if(len(self.shifts)>0):
            if(self.shifts[-1] != shift):
                if(self.shifts[-1].end_time < shift.start_time):
                    return True
                else:
                    #print('conflict back-to-back shifts')
                    return False
            else:
                #print('conflict same shift')
                return False
        else:
            return True

    def __str__(self):
        return self.first_name+' '+self.last_name

