from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
import pandas as pd
import numpy as np
from .builder import process_shifts

def check_valid(df,columns):
    if((not df.empty) or (not df.isnull().values.any()) or np.array_equal(np.sort(columns,axis=0), np.sort(df.columns,axis=0))):
        return True
    return False



def process_files(employees,shifts):
    result = 'Invalid data. Please try again.'
    roster = False
    try:
        employees = pd.read_csv(employees)
        shifts = pd.read_csv(shifts)
        if(check_valid(employees,['First Name', 'Last Name']) and check_valid(shifts,['Date', 'Start', 'End', 'Break'])):
            roster = process_shifts(employees,shifts)
            if(len(roster) == 0):
                roster = False
                result = 'Sorry a roster could not be built because of less capacity or wrong format. Reduce shifts or increase employees and check the format.'
        else:
            result = 'Recheck files for no contents, all necessary columns and/or any missing values.'
    except pd.io.common.EmptyDataError:
        result = 'Invalid data. Please try again.'
    return roster,result

def home(request):
    if request.method == 'POST':
        if len(request.FILES) < 2:
            return render(request, 'home.html', {
                'error_text': 'Please upload both the files.'
            })
        if 'employees' in request.FILES and 'shifts' in request.FILES:
            roster,result = process_files(request.FILES['employees'],request.FILES['shifts'])
            if(roster):
                return render(request, 'home.html', {
                    'roster': roster
                })
            else:
                return render(request, 'home.html', {
                    'error_text': result
                })
        return render(request, 'home.html', {
            'error_text': 'Files are invalid.'
        })
    else:
        return render(request, 'home.html')
